# React Pentaho Renderer

> Note: This is a work under development and didn't reach yet a stable stage.

This is a component that renders Pentaho reports, analysiss views and dashboards into a ReactJS App.

``

This component for now takes care of authenticating the user, but this should move to another component in the future.

```jsx
<PentahoArtifact 
    match={{params:id}}  /*This must be btoa(`${type}||${url}`)*/
/>
```

This implementation still has `"http://localhost:8080/pentaho/"` hardcoded, but this param will be exposed.

There are two _types_ of pentaho artifacts: `[iframe|requirejs]`. The RequireJS however is still very stable.

The url is the _url_ after the forward slash on the pentaho url.

When invoked, the component:

* Checks if there's a valid Pentaho session;
* Authenticates the user - cookie based type, still hardcoded to `admin/password`;
* Imports the requirejs dependencies from Pentaho
* Renders an iframe with the given url; or
* Renders the RequireJS Dashboard on its placeholder.

# Usage

This implementation is still strongly attached to an example that uses [react-router](https://github.com/ReactTraining/react-router) and passes the `id` mentioned above as parameter with this variable name, such as:

```jsx
<Route 
    path="/pentaho-report/:id" 
    component={PentahoArtifact} />
```

In the component we're using `this.props.match.params.id` to identify type and url as mentioned earlier.