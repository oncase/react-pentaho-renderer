import React, { Component } from 'react';
import PropTypes from 'prop-types';
import postscribe from 'postscribe';
import { queryParams } from './helpers';
import PentahoPlaceHolder from './PentahoPlaceHolder';

class PentahoArtifact extends Component {
  constructor(props) {
    super(props);

    const hasDependencies = window.requireCfg !== undefined;

    this.state = {
      hasDependencies
    };

    this.dispatchSciptImport();
  }

  authenticate() {
    const { pentahoUrl } = this.props;
    const login = {
      j_username: 'admin',
      j_password: 'password'
    };

    fetch(`${pentahoUrl}j_spring_security_check`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: queryParams(login),
      mode: 'cors',
      credentials: 'include'
    })
      .then(res => {
        const statusOK = res.status === 200;
        const successfulLogin = !res.url.includes('?login_error=');

        if (statusOK && successfulLogin) {
          this.writeScriptTag(pentahoUrl);
        } else {
          console.error('error logging in', res, window.requireCfg);
        }
      })
      .catch(err => console.error(err));
  }

  writeScriptTag(url) {
    postscribe(
      '#root',
      `<script src="${url}plugin/pentaho-cdf-dd/api/renderer/cde-embed.js"></script>`,
      { done: this.scriptLoadedHandler.bind(this) }
    );
  }

  scriptLoadedHandler() {
    window.require.config({
      config: {
        text: {
          useXhr: (url, protocol, hostname, port) => {
            // allow cross-domain requests
            // remote server allows CORS
            return true;
          }
        }
      }
    });

    this.setState({ hasDependencies: true });
  }

  dispatchSciptImport() {
    const { pentahoUrl } = this.props;

    fetch(`${pentahoUrl}Home`, {
      mode: 'cors',
      credentials: 'include'
    })
      .then(res => {
        const statusOK = res.status === 200;
        const successfulLogin = res.url.endsWith('/Home');

        // logged in
        if (statusOK && successfulLogin) {
          this.writeScriptTag(pentahoUrl);
        } else {
          // need to login
          this.authenticate();
        }
      })
      .catch(err => this.authenticate());
  }

  renderLoading() {
    return <div>Loading...</div>;
  }

  renderEmbed() {
    const { match, pentahoUrl } = this.props;
    const id = match.params.id || match.params;
    const decodedID = window.atob(id).split('||');
    const type = decodedID[0];
    const url = decodedID[1];
    const childProps = { type, pentahoUrl, url };

    return <PentahoPlaceHolder {...childProps} />;
  }

  render() {
    return !this.state.hasDependencies
      ? this.renderLoading()
      : this.renderEmbed();
  }
}

PentahoArtifact.propTypes = {
  match: PropTypes.object.isRequired,
  pentahoUrl: PropTypes.string
};

PentahoArtifact.defaultProps = {
  pentahoUrl: 'http://localhost:8080/pentaho/'
};

export default PentahoArtifact;
