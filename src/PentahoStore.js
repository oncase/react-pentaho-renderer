class PentahoStore {
  static dashboards = {};

  static add(id, obj) {
    let temp = {};

    temp[id] = obj;
    this.dashboards = Object.assign({}, this.dashboards, temp);
  }

  static get(id) {
    return this.dashboards[id];
  }

  static has(id) {
    return this.get(id) !== undefined;
  }
}

export default PentahoStore;
